import React from 'react';
import PropTypes from 'prop-types';
import {View, FlatList, StyleSheet} from 'react-native';
import DealItem from './DealItem';

class DealList extends React.Component {
  static propTypes = {
    deals: PropTypes.array.isRequired,
    onItemPress: PropTypes.func.isRequired,
  };
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.deals}
          renderItem={({item}) => <DealItem deal={item} onPress={this.props.onItemPress} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
});

export default DealList;
