import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {priceDisplay} from '../util/util';

class DealItem extends React.Component {
  static propTypes = {
    deal: PropTypes.object.isRequired,
    onPress: PropTypes.func.isRequired,
  };

  handlePress = () =>{
    this.props.onPress(this.props.deal.key);
  };
 
  render() {
    const {deal} = this.props;
    return (
      <TouchableOpacity style={styles.deal} onPress={this.handlePress}>
        <Image style={styles.image} source={{uri: deal.media[0]}} />
        <View style={styles.info}>
          <Text style={styles.title}>{deal.title}</Text>
          <View style={styles.footer}>
            <Text style={styles.cause}>{deal.cause.name}</Text>
            <Text style={styles.price}>{priceDisplay(deal.price)}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  deal: {
    marginHorizontal: 4,
    marginTop: 12,
  },
  image: {
    width: '100%',
    height: 160,
    alignContent: 'stretch',
    backgroundColor: '#ccc',
  },
  info: {
    padding: 10,
    backgroundColor: '#4798F7', // 'rgba(237,149,245,0.4)',
    borderColor: '#4798F7',
    borderWidth: 2,
    borderTopWidth: 0,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#ffffff',
  },
  footer: {
    flexDirection: 'row',
  },
  cause: {
    flex: 2,
    color: '#ffffff',
  },
  price: {
    flex: 1,
    textAlign: 'right',
    fontWeight: 'bold',
    color: '#ffffff',
  },
});

export default DealItem;
