import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Button,
  Linking,
  ScrollView,SafeAreaView,
} from 'react-native';
import {priceDisplay} from '../util/util';
import api from '../components/api';

class DealDetail extends React.Component {
  static propTypes = {
    initialDealData: PropTypes.object.isRequired,
    onBack: PropTypes.func.isRequired,
  };
  state = {
    deal: this.props.initialDealData,
    imageIndex: 0,
  };
  async componentDidMount() {
    const fullDeal = await api.fetchDealDetail(this.state.deal.key);
    this.setState({deal: fullDeal});
  }
  openDealUrl = () =>{
    Linking.openURL(this.state.deal.url);
  }
  render() {
    const {deal} = this.state;
    return (
      <SafeAreaView style={{flex: 1}}>
      <ScrollView style={styles.deal}>
        <TouchableOpacity style={styles.backLink} onPress={this.props.onBack}>
          <Text style={styles.backLinkText}>Back</Text>
        </TouchableOpacity>
        <Image
          style={styles.image}
          source={{uri: deal.media[this.state.imageIndex]}}
        />
        <View style={styles.detail}>
          <Text style={styles.title}>{deal.title}</Text>
          <View style={styles.footer}>
            <View style={styles.info}>
              <View style={styles.infoItem}>
                <Text style={styles.baseText}>Price:</Text>
                <Text style={styles.price}>{priceDisplay(deal.price)}</Text>
              </View>
              <View style={styles.infoItem}>
                <Text style={styles.baseText}>Type:</Text>
                <Text style={styles.dynamicText}>{deal.dealType}</Text>
              </View>
              <View style={styles.infoItem}>
                <Text style={styles.baseText}>Cause:</Text>
                <Text style={styles.dynamicText}>{deal.cause.name}</Text>
              </View>
            </View>
            {deal.user && (
              <View style={styles.user}>
                <Image source={{uri: deal.user.avatar}} style={styles.avatar} />
                <Text style={styles.baseText}>{deal.user.name}</Text>
              </View>
            )}
          </View>
        </View>
        <View style={styles.description}>
          <Text style={styles.baseText}>{deal.description}</Text>          
        </View>
      </ScrollView>
      <View style={styles.headerFooterStyle}>
            <TouchableOpacity style={styles.backLink} onPress={this.openDealUrl}><Text style={styles.headerFooterStyleText}>Buy this deal!</Text>
        </TouchableOpacity>            
      </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  backLink: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,    
  },
  backLinkText: {
    fontSize: 16,
    color:"#76bf46",
  },
  image: {
    width: '100%',
    height: 160,
    backgroundColor: '#ccc',
  },
  title: {
    fontSize: 16,
    padding: 10,
    fontWeight: 'bold',
    backgroundColor: '#4798F7', // 'rgba(237,149,245,0.4)',
    color: '#ffffff',
  },  
  footer: {
    backgroundColor:'#f7c845', //'rgba(237,149,45,0.4)',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  info: {
    alignItems: 'flex-start',
  },
  infoItem: {
    flexDirection: 'row',
    marginTop: 7,
    justifyContent: 'center',
    alignItems: 'flex-start',    
  },
  user: {
    alignItems: 'center',
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  price: {
    paddingHorizontal: 2,
    fontWeight: 'bold',
    color: '#323232',
  },
  dynamicText: {
    paddingHorizontal: 2,
    color: '#323232',
  },
  description: {
    marginTop: 10,
    marginHorizontal: 5,    
  },
  baseText: {
    fontSize: 14,
    color: '#323232',
  },
  headerFooterStyle: {
    height: 40,
    alignItems: 'center',
  },
  headerFooterStyleText: {
    fontSize: 16,
    color: '#76bf46',
  },
});

export default DealDetail;
